# Php cơ bản
## Yêu cầu
- Các kiến thức tối thiểu phải lắm được: 
 - PHP cơ bản      
  - Introduction to web Technology     
  - Introduction to HTML5 and CSS     
  - PHP introduction     
  - PHP syntax     
  - PHP Data Types     
  - PHP Variables ,Constants and Array     
  - PHP Operators and Control Statements         
    - if else statement         
    - else if statement         
    - switch case         
    - Loops            
    - while            
    - do while            
    - for            
    - foreach      
  - String Functions      
  - Array Functions 


## Kien thuc thu duoc
- Cú pháp khai báo php
- Biến, hằng và mảng trong php
- các kiểu dữ liệu trong php
- câu lệnh điều kiện và các vòng lặp trong php
- cấu trúc switch case
- hàm và cách tạo hàm xử lý mảng và chuỗi

  
Thực hiện bởi [Nguyen Hoang Anh](https://github.com/anhbk177)

## Liên kết
 
[hoanganh-html-css](https://gitlab.com/123doc-trainese/hoanganh-htmlcss)
